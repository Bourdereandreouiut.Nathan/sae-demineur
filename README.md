# SAE 2.01 - Jeu du démineur

Fait par :

- Bourdere Andreou Nathan (Chef de groupe)
- Viard Theo
- Blin–Dorard Bryan

## Sujet

Dans ce projet, nous devions développer une application JAVA qui reprodusait le jeu du Démineur.
Pour rappel, Le Démineur est un jeu dont le but est de découvrir
toutes les cases libres sans faire exploser les mines.

## Fonctionnalités supplémentaires


- Choisir la difficulté
- Pouvoir gagner avec les drapeaux
- Relancer le jeu après une partie
- Avoir des couleurs sur les numéros
- Chronométrer le temps d'une partie
- Effondrer les cases voisine lors de la découverte d'une case


## Utilitaires

Ce que nous avons utilisé pour créer le jeu :

- [Java] - Un langage de programmation orienté Objet
- [Vscode] - Un éditeur de code
- [JavaFX] - Une librairie permettant aux développeurs Java de créer une interface graphique

## Lancement

Pour lancer notre jeu il vous faut [Java] et [JavaFX].

Il vous faut ensuite utiliser cette commande pour compiler le projet :
```sh
javac -d bin --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls ./src/*.java
```
***/usr/share/openjfx/lib/*** étant le chemin où sont situés les fichiers de la librairie JavaFX.

Ensuite pour lancer le jeu, vous devez faire :
```sh
java -cp bin:img --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls DemineurGraphique
```

   [Java]: https://www.java.com/fr/
   [Vscode]: https://code.visualstudio.com
   [JavaFX]:https://openjfx.io
