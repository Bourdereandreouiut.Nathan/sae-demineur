import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.RadioButton;
public class ControleurDifficulte implements EventHandler<ActionEvent> {
    private DemineurGraphique demineur;

    /**
     * Constructeur de la classe ControleurDifficulte
     * @param vue La vue du jeu du démineur
     */
    public ControleurDifficulte(DemineurGraphique vue){
        this.demineur = vue;
    }

    /**
     * En cas de choix de la difficulté par un clic sur le radioButton
     */
    @Override
    public void handle(ActionEvent event){
        RadioButton rb = (RadioButton) event.getTarget();
        switch(rb.getText()){
            case "Facile":
            this.demineur.setDifficulte(Plateau.FACILE);
            this.demineur.lancePartie();
            break;
            case "Normal":
            this.demineur.setDifficulte(Plateau.NORMAL);
            this.demineur.lancePartie();
            break;
            case "Difficile":
            this.demineur.setDifficulte(Plateau.DIFFICILE);
            this.demineur.lancePartie();
            break;
            case "Expert":
            this.demineur.setDifficulte(Plateau.EXPERT);
            this.demineur.lancePartie();
            break;
        }
    }

}
