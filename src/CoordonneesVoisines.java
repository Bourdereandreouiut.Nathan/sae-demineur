import java.util.List;
import java.util.Arrays;

public class CoordonneesVoisines {
    //* classe de constantes (ou attributs statiques) des coordonnées voisines d'une case.*/
    public int x;
    public int y;
    public static CoordonneesVoisines HAUT= new CoordonneesVoisines(0,-1);
    public static CoordonneesVoisines BAS = new CoordonneesVoisines(0,1);
    public static CoordonneesVoisines GAUCHE = new CoordonneesVoisines(-1, 0);
    public static CoordonneesVoisines DROITE = new CoordonneesVoisines(1,0);
    public static CoordonneesVoisines HAUT_GAUCHE = new CoordonneesVoisines(-1, -1);
    public static CoordonneesVoisines HAUT_DROITE = new CoordonneesVoisines(1, -1);
    public static CoordonneesVoisines BAS_GAUCHE = new CoordonneesVoisines(-1, 1);
    public static CoordonneesVoisines BAS_DROITE = new CoordonneesVoisines(1,1);
    public static List<CoordonneesVoisines> LISTE_COORD = Arrays.asList(CoordonneesVoisines.HAUT_GAUCHE, CoordonneesVoisines.HAUT, CoordonneesVoisines.HAUT_DROITE, CoordonneesVoisines.GAUCHE, CoordonneesVoisines.DROITE, CoordonneesVoisines.BAS_GAUCHE, CoordonneesVoisines.BAS, CoordonneesVoisines.BAS_DROITE);

    /**
     * Constructeur de la classe CoordonneesVoisines
     * @param x Coordonnée x d'une position sur le plateau
     * @param y Coordonnée y d'une position sur le plateau
     */
    public CoordonneesVoisines(int x,int y){
        this.x = x;
        this.y = y;
    }

    // Getters
    public int getX(){return this.x;}
    public int getY(){return this.y;}
}
